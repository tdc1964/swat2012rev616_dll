      !!include 'modparm.f'

      subroutine swat2012(bsn,
     & cio,
     & soil_i,soil_f,soil_c,layers,
     & sub_i,sub_f,
     & hru_i,hru_f,
     & mgt_h_i,mgt_h_f,mgt_i,mgt_f,
     & wgn_f,wgn_mon_f,
     & fig_i,fig_f,
     & pcs_f, !! point sources
     & res,lwq,
     & pond_i,pond_f,
     & rte_swq_i,rte_swq_f,
     & wwq_i,wwq_f,
     & pcp_f,tmp_f,slr_f,hmd_f,wnd_f,
     & wus_f,
     & crops, n_crop,
     & ferts, n_fert,
     & pests, n_pest,
     & tillages,n_till,
     & urbans,n_urban,
     & shyd,
     & outrch_d)
     & BIND(C, name='swat2012')
      
!!    this is the main program that reads input, calls the main simulation
!!    model, and writes output.
!!    ~ ~ ~ INCOMING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!         ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
!!    date        |NA            |date simulation is performed where leftmost
!!                               |eight characters are set to a value of
!!                               |yyyymmdd, where yyyy is the year, mm is the 
!!                               |month and dd is the day
!!    isproj      |none          |special project code:
!!                               |1 test rewind (run simulation twice)
!!    time        |NA            |time simulation is performed where leftmost
!!                               |ten characters are set to a value of
!!                               |hhmmss.sss, where hh is the hour, mm is the 
!!                               |minutes and ss.sss is the seconds and
!!                               |milliseconds
!!    values(1)   |year          |year simulation is performed
!!    values(2)   |month         |month simulation is performed
!!    values(3)   |day           |day in month simulation is performed
!!    values(4)   |minutes       |time difference with respect to Coordinated
!!                               |Universal Time (ie Greenwich Mean Time)
!!    values(5)   |hour          |hour simulation is performed
!!    values(6)   |minutes       |minute simulation is performed
!!    values(7)   |seconds       |second simulation is performed
!!    values(8)   |milliseconds  |millisecond simulation is performed
!!    zone        |NA            |time difference with respect to Coordinated
!!                               |Universal Time (ie Greenwich Mean Time)
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 

!!    ~ ~ ~ OUTGOING VARIABLES ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
!!    prog        |NA            |program name and version
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 

!!    ~ ~ ~ LOCAL DEFINITIONS ~ ~ ~
!!    name        |units         |definition
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
!!    i           |none          |counter
!!    ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 


!!    ~ ~ ~ SUBROUTINES/FUNCTIONS CALLED ~ ~ ~
!!    Intrinsic: date_and_time
!!    SWAT: getallo, allocate_parms, readfile, readfig
!!    SWAT: readbsn, std1, readwwq, readinpt, std2, storeinitial
!!    SWAT: openwth, headout, simulate, finalbal, writeaa, pestw 

!!    ~ ~ ~ ~ ~ ~ END SPECIFICATIONS ~ ~ ~ ~ ~ ~

!! tdc 2017-03-22 new section
      use iso_c_binding
      use modtype
      use parm
      use ifport  !! for chdir
      implicit none
      
      !DEC$ ATTRIBUTES DLLEXPORT::swat2012
      
      !! INTEGER(C_INT), INTENT(IN), VALUE :: path_len
      !!CHARACTER (kind=c_char), DIMENSION(path_len), INTENT(in)
     !!& :: path

      !Yuzhou/Ficklin/Barnhart 2016-01-20
      TYPE(BSNTYP), INTENT (IN) :: bsn [REFERENCE]
      TYPE(CIOTYP), INTENT (IN) :: cio [REFERENCE]
      INTEGER(C_INT), DIMENSION(2,cio%n_soil),
     & INTENT(IN) :: soil_i [REFERENCE]
      REAL (C_FLOAT), DIMENSION(3,cio%n_soil),
     & INTENT(IN) :: soil_f [REFERENCE]
      CHARACTER (C_CHAR), DIMENSION(cio%n_soil*16),
     & INTENT(IN) :: soil_c [REFERENCE]
      REAL (C_FLOAT), DIMENSION(17,cio%n_layer),
     & INTENT(IN) :: layers [REFERENCE]
      INTEGER(C_INT), DIMENSION (9,cio%n_sub),
     & INTENT(IN) :: sub_i [REFERENCE] !!  1:WGN index, 2-6: historical weather indices, 7:hrutot, 8:pond index, 9:WUS index
      REAL(C_FLOAT), DIMENSION(cio%sub_f_w,cio%n_sub),
     & INTENT(IN) :: sub_f [REFERENCE]
      INTEGER(C_INT), DIMENSION(cio%hru_i_w,cio%n_hru),
     & INTENT(IN) :: hru_i [REFERENCE]  !! # of ops, soil array offset, soil layer array offset, Septic ID, septic year, septic type,pothole flag, # TCP ops
      REAL(C_FLOAT), DIMENSION(83,cio%n_hru),
     & INTENT(IN) :: hru_f [REFERENCE]
      INTEGER(C_INT), DIMENSION(7,cio%n_hru),
     & INTENT(IN) :: mgt_h_i [REFERENCE]
      REAL(C_FLOAT), DIMENSION(14,cio%n_hru),
     & INTENT(IN) :: mgt_h_f [REFERENCE]

      INTEGER(C_INT), INTENT(IN), VALUE :: n_crop
      TYPE(CROPTYP), DIMENSION(n_crop),
     & INTENT (IN) :: crops [REFERENCE]
      INTEGER(C_INT), INTENT(IN),VALUE :: n_fert
      TYPE(FERTTYP), DIMENSION(n_fert),
     & INTENT (IN) :: ferts [REFERENCE]
      INTEGER(C_INT), INTENT(IN), VALUE :: n_pest
      TYPE(PESTTYP), DIMENSION(n_pest),
     & INTENT (IN) :: pests [REFERENCE]
      INTEGER(C_INT), INTENT(IN), VALUE :: n_till
      TYPE(TILLTYP), DIMENSION(n_till),
     & INTENT (IN) :: tillages [REFERENCE]
      INTEGER(C_INT), INTENT(IN), VALUE :: n_urban
      TYPE(URBANTYP), DIMENSION(n_urban),
     & INTENT (IN) :: urbans [REFERENCE]
 
      INTEGER(C_INT), DIMENSION(7,cio%n_op),
     & INTENT(IN) :: mgt_i [REFERENCE]
      REAL(C_FLOAT), DIMENSION(7,cio%n_op),
     & INTENT(IN) :: mgt_f [REFERENCE]
      REAL(C_FLOAT), DIMENSION(3,cio%n_wgn),
     & INTENT(IN) :: wgn_f  [REFERENCE] ! weather station data
      REAL(C_FLOAT), DIMENSION(12,cio%n_wgn*14),
     & INTENT(IN) :: wgn_mon_f  [REFERENCE] ! monthly ave weather data

      INTEGER(C_INT), DIMENSION(9,cio%max_hsl),
     & INTENT(IN) :: fig_i [REFERENCE]
      REAL(C_FLOAT), DIMENSION(cio%max_hsl),
     & INTENT(IN) :: fig_f [REFERENCE]
      REAL(C_FLOAT), DIMENSION(18,cio%n_pcs),
     & INTENT(IN) :: pcs_f [REFERENCE]
      TYPE(RESTYP), DIMENSION(cio%n_res_lwq),
     & INTENT (IN) :: res [REFERENCE]
      TYPE(LWQTYP), DIMENSION(cio%n_res_lwq),
     & INTENT (IN) :: lwq [REFERENCE]

      INTEGER(C_INT), DIMENSION(5,cio%n_pond),
     & INTENT(IN) :: pond_i [REFERENCE]
      REAL (C_FLOAT), DIMENSION(41,cio%n_pond),
     & INTENT(IN) :: pond_f [REFERENCE]

      INTEGER(C_INT), DIMENSION(1,cio%n_rte),
     & INTENT(IN) :: rte_swq_i [REFERENCE]
      REAL(C_FLOAT), DIMENSION(61,cio%n_rte),
     & INTENT(IN) :: rte_swq_f [REFERENCE]

      REAL (C_FLOAT), DIMENSION(48,cio%n_wus),
     & INTENT(IN) :: wus_f [REFERENCE]

      INTEGER(C_INT), DIMENSION(2), INTENT(IN) :: wwq_i [REFERENCE]
      REAL(C_FLOAT), DIMENSION(18), INTENT(IN) :: wwq_f [REFERENCE]

      REAL(C_FLOAT),DIMENSION(cio%nrtot,cio%n_day),
     & INTENT(IN) :: pcp_f [REFERENCE]
      REAL(C_FLOAT),DIMENSION(cio%nttot*2,cio%n_day),
     & INTENT(IN) :: tmp_f [REFERENCE]
      REAL(C_FLOAT),DIMENSION(cio%nstot,cio%n_day),
     & INTENT(IN) :: slr_f [REFERENCE]
      REAL(C_FLOAT),DIMENSION(cio%nhtot,cio%n_day),
     & INTENT(IN) :: hmd_f [REFERENCE]
      REAL(C_FLOAT),DIMENSION(cio%nwtot,cio%n_day),
     & INTENT(IN) :: wnd_f [REFERENCE]

      REAL(C_FLOAT), DIMENSION(8,cio%max_hsl),
     & INTENT(OUT) :: shyd [REFERENCE]
      REAL(C_FLOAT), DIMENSION(14,cio%n_day),
     & INTENT(OUT) :: outrch_d [REFERENCE]
      INTEGER :: i_ga, j_ga, iii_ga, i_status
!! tdc 2017-03-22 end of new section      
     
      prog = "SWAT Jan 22 2014    VER 2012/Rev 615"
      write (*,1000)
 1000 format(1x,"               SWAT2012               ",/,             
     &          "               Rev. 615              ",/,             
     &          "      Soil & Water Assessment Tool    ",/,             
     &          "               PC Version             ",/,             
     &          " Program reading from file.cio . . . executing",/)
      
	! allocate(swatout_ga(nobs, 14))
	!allocate(frt_ga(nvars))
      !allocate (hrunum2_ga (nhrus_ga))

!! assign GA values
      
!! process input
		
!! tdc 2017-03-22 new section
      i_status = chdir (cio%path)
      
!!      !$OMP parallel
!!      !$OMP DO
      call getallo(bsn,cio,mgt_i,fig_i,
     & hru_i,
     & sub_i,crops,n_crop,
     & ferts, n_fert,
     & pests, n_pest,
     & tillages,n_till,urbans,n_urban)
      
     
      call allocate_parms
      call readfile(cio)
      call readbsn(bsn)
      if(cio%wwq==1) call readwwq(wwq_i,wwq_f)
      if (fcstyr > 0 .and. fcstday > 0) call readfcst
      call readplant (crops,n_crop)  !! read in the landuse/landcover database
      call readtill (tillages,n_till)              !! read in the tillage database
      call readpest (pests,n_pest)             !! read in the pesticide database
      call readfert(ferts,n_fert)              !! read in the fertilizer/nutrient database
      call readurban(urbans,n_urban)       !! read in the urban land types database
      call readseptwq            !! read in the septic types database     
      call readlup
      call readfig(cio,soil_i,soil_f,soil_c,
     & layers,
     & fig_i,fig_f,
     & pcs_f,
     & res,lwq,
     & rte_swq_i,rte_swq_f,
     & sub_i,sub_f,
     & hru_i,hru_f,
     & mgt_h_i,mgt_h_f,mgt_i,mgt_f,
     & pond_i,pond_f,
     & wgn_f,wgn_mon_f,
     & wus_f)
      call readatmodep
      call readinpt
      call std1
      call std2
      call openwth(cio)
      call headout
!!      call show_input
!! tdc 2017-03-22 end of new section

      !! convert integer to string for output.mgt file
      subnum = ""
      hruno = ""
      do i = 1, mhru
        write (subnum(i),fmt=' (i5.5)') hru_sub(i)
        write (hruno(i),fmt=' (i4.4)') hru_seq(i)  
      end do

      if (isproj == 2) then 
        hi_targ = 0.0
      end if

!! save initial values
      if (isproj == 1) then
        scenario = 2
        call storeinitial
      else if (fcstcycles > 1) then
        scenario =  fcstcycles
        call storeinitial
      else
        scenario = 1
      endif
        if (iclb /= 4) then
      do iscen = 1, scenario

     
        !! simulate watershed processes
        call simulate(cio,shyd,
     &  pcp_f,tmp_f,slr_f,hmd_f,wnd_f,outrch_d)        

        !! perform summary calculations
        call finalbal
        call writeaa
        call pestw

        !!reinitialize for new scenario
        if (scenario > iscen) call rewind_init
      end do
         end if
      do i = 101, 109       !Claire 12/2/09: change 1, 9  to 101, 109.
        close (i)
      end do
      close(124)
      write (*,1001)
 1001 format (/," Execution successfully completed ")
	
        iscen=1
!! file for Mike White to review to ensure simulation executed normally
      open (9999,file='fin.fin')
      write (9999,*) 'Execution successful'
      close (9999)
      
      !barnhart clean-up.
      do i = 1, 9999
        close(i)
      end do
      close (77778) ! bmp-sedfil.out
      close (77779) ! bmp-ri.out
      
      ! close hyd.out
      !! close (11123)
      
     
     
      !deallocate(swatout_ga)
      !deallocate(frt_ga)
      !deallocate(hrunum2_ga)
      
!! tdc 2017-03-27 section added
      call deallocate_parms
!! end of section added      
!!      !$OMP end do
!!      !$OMP end parallel

      end subroutine swat2012
      