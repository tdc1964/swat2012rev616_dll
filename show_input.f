      subroutine show_input

      use parm

      integer :: sb, ic, i_hru, i_op, ii
      real :: sub_ha
      real, dimension (msubo) :: pdvab, pdvb
      character(len=13) :: c_hed(39)
      character :: tab
      integer :: show_section (6)
      integer :: station_ids (5)

      tab = char(9)

      show_section(1) = 0 ! HRU
      show_section(2) = 0 ! MGT
      show_section(3) = 0 ! Soil
      show_section(4) = 1 ! BSN
      show_section(5) = 1 ! WWQ
      show_section(6) = 1 ! Subbasin weather ids

      open (575,file='dll_input.txt')


      if (show_section(1) == 1) then
!!      column headers for HRU output file
        c_hed = (/"Name      ","       idc","     bio_e","     hvsti",
     &          "      blai","    *frgw1","   *laimx1","   *frgrw2",
     &          "   *laimx2","      dlai","     chtmx","      rdmx",
     &          "     t_opt","    t_base","     cnyld","     cpyld",
     &          " pltnfr(1)"," pltnfr(2)"," pltnfr(3)"," pltpfr(1)",
     &          " pltpfr(2)"," pltpfr(3)","      wsyf","    usle_c",
     &          "       gsi","    *vpdfr","   *frgmax","      wavp",
     &          "    *co2hi","   *bioehi","  rsdco_pl","  alai_min",
     &          "  bio_leaf","   mat_yrs"," bmx_trees","  ext_coef",
     &          " bm_dieoff","      rsr1","      rsr2"/)
         write (575,1000) (c_hed(j), j = 1, 39) 

         do ic=1,mcrdb
         if (idc(ic) .GT. 0) then
           write (575,1001) cpnm(ic),idc(ic),bio_e(ic), hvsti(ic),
     &     blai(ic),0.0, 0.0, 0.0, 0.0,dlai(ic),chtmx(ic),
     &     rdmx(ic),t_opt(ic), t_base(ic), cnyld(ic), cpyld(ic),
     &     pltnfr(1,ic), pltnfr(2,ic), pltnfr(3,ic), pltpfr(1,ic),
     &     pltpfr(2,ic), pltpfr(3,ic),wsyf(ic), 0.0, gsi(ic), 0.0,
     &     0.0, wavp(ic), 0.0, 0.0, rsdco_pl(ic), alai_min(ic),
     &     bio_leaf(ic), 
     &     mat_yrs(ic),
     &     bmx_trees(ic), ext_coef(ic), bm_dieoff(ic),
     &     rsr1(ic), rsr2(ic)
          end if

        end do
      end if

      if (show_section(2) == 1) then
        ! Management
        write (575,5201) "HRU ",tab,"JD",tab,"HUSC",tab,"OP",
     &  tab,"MGT1",tab,"MGT2",tab,"MGT3",tab,"MGT4",
     &  tab,"MGT5"
       do i_hru=1,mhru
         do i_op = 1,nopmx(i_hru)
           if (mgtop(i_op,i_hru) .NE. 17) then
             write(575,5200) i_hru,
     &       tab,idop(i_op,i_hru),
     &       tab,phu_op(i_op,i_hru),
     &       tab,mgtop(i_op,i_hru), tab,mgt1iop(i_op,i_hru),
     &       tab,mgt2iop(i_op,i_hru),
     &       tab,mgt3iop(i_op,i_hru), tab,mgt4op(i_op,i_hru),
     &       tab,mgt5op(i_op,i_hru),
     &       tab,mgt6op(i_op,i_hru), tab,mgt7op(i_op,i_hru),
     &       tab,mgt8op(i_op,i_hru),
     &       tab,mgt9op(i_op,i_hru), 
     &       tab,mgt10iop(i_op,i_hru)
           end if
         end do
       end do
      end if

      if (show_section(3) == 1) then
        !! Soil Layers
        do i_hru=1,mhru
          write (575,"(i4)") i_hru

          write (575,"(*(f12.2,:,a))")
     &    (sol_z(j,i_hru), tab, j=1,sol_nly(i_hru))
          write (575,"(*(f12.2,:,a))")
     &    (sol_bd(j,i_hru),tab, j=1,sol_nly(i_hru))
          write (575,"(*(f12.2,:,a))")
     &    (sol_awc(j,i_hru),tab, j=1,sol_nly(i_hru))
          write (575,"(*(f12.2,:,a))")
     &    (sol_k(j,i_hru),tab, j=1,sol_nly(i_hru))
          write (575,"(*(f12.2,:,a))")
     &    (sol_cbn(j,i_hru),tab, j=1,sol_nly(i_hru))
          write (575,"(*(f12.2,:,a))")
     &    (sol_clay(j,i_hru),tab, j=1,sol_nly(i_hru))
          write (575,"(*(f12.2,:,a))")
     &    (sol_silt(j,i_hru),tab, j=1,sol_nly(i_hru))
          write (575,"(*(f12.2,:,a))")
     &    (sol_sand(j,i_hru),tab, j=1,sol_nly(i_hru))
          write (575,"(*(f12.2,:,a))")
     &    (sol_rock(j,i_hru),tab, j=1,sol_nly(i_hru))
          write (575,"(f12.2)") sol_alb(i_hru)
          write (575,"(f12.2)") usle_k(i_hru)
          write (575,"(*(f12.2,:,a))")
     &    (sol_ec(j,i_hru),tab, j=1,sol_nly(i_hru))
          write (575,"(*(f12.2,:,a))")
     &    (sol_cal(j,i_hru),tab, j=1,sol_nly(i_hru))
          write (575,"(*(f12.2,:,a))")
     &    (sol_ph(j,i_hru),tab, j=1,sol_nly(i_hru))

        end do
      end if

      if (show_section(4) == 1) then
        ! BSN
        write(575,*) "BSN"
        write (575,"(*(f12.4,:,a))") sftmp,tab,smtmp,tab,
     &   smfmx,tab,smfmn,tab
        write (575,"(*(f12.4,:,a))") timp,tab,snocovmx,tab,
     &   sno50cov,tab
        write (575,"(i4)") ipet

        ! escobsn/epcobsn values are copied to esco/epco, arrays by HRU
        ! they could have been changed by hru values in readhru
        write (575,"(*(f12.4,:,a))") esco(1),tab,epco(1),tab,
     &   evlai,tab,ffcb,tab
        write (575,"(*(i4,:,a))") ievent,tab,icrk,tab
        write (575,"(*(f12.4,:,a))") surlag,tab,adj_pkr,tab,
     &   prf,tab,spcon,tab
        write (575,"(*(f12.4,:,a))") spexp,tab,rcn_sub_bsn,tab,
     & cmn,tab,n_updis,tab,p_updis,tab,nperco,tab,pperco,tab
        write (575,"(*(f12.4,:,a))") phoskd,tab,psp,tab,
     & rsdco,tab,percop,tab
        write (575,"(i4)") isubwq

        write (575,"(*(f12.4,:,a))") wdpq,tab,wgpq,tab,
     & wdlpq,tab,wglpq,tab,wdps,tab,wgps,tab,wdlps,tab,wglps,tab

        write (575,"(*(f12.4,:,a))") bactkdq,tab,thbact,tab,
     &   wof_p,tab,wof_lp,tab,wdpf,tab,wgpf,tab,wdlpf,tab,wglpf,tab

        write (575,"(*(i4,:,a))") ised_det,tab,irte,tab
        write (575,"(*(f12.4,:,a))") msk_co1,tab,msk_co2,tab,
     &   msk_x,tab
        write (575,"(*(i4,:,a))") ideg,tab,iwq,tab
        write (575,"(*(f12.4,:,a))") trnsrch,tab,evrch,tab
        write (575,"(*(i4,:,a))") irtpest,tab,icn,tab

        write (575,"(*(f12.4,:,a))") cncoef,tab,cdn,tab,
     &   sdnco,tab,bact_swf,tab,bactmx,tab,bactminlp,tab,
     &   bactminp,tab,wdlprch,tab,wdprch,tab,wdlpres,tab,
     &   tb_adj,tab,depimp_bsn,tab

        write (575,"(*(i4,:,a))") ddrain_bsn,tab,tdrain_bsn,tab,
     &   gdrain_bsn,tab

        write (575,"(*(f12.4,:,a))") cn_froz,tab,dorm_hr,tab,
     &   smxco,tab,fixco,tab,nfixmx,tab,anion_excl_bsn,tab,
     &   ch_onco_bsn,tab,ch_opco_bsn,tab,hlife_ngw_bsn,tab,
     &   bc1_bsn,tab,bc2_bsn,tab,bc3_bsn,tab,bc4_bsn,tab,
     &   decr_min,tab

        write (575,"(i4)") icfac
        write (575,"(*(f12.4,:,a))") rsd_covco,tab,vcrit,tab,
     &   cswat,tab,res_stlr_co,tab,bf_flg,tab
        write (575,"(i4)") iuh
        write (575,"(*(f12.4,:,a))") uhalpha,tab,eros_spl,tab,
     &   rill_mult,tab,eros_expo,tab,sed_ch,tab,c_factor,tab,
     &   ch_d50,tab,sig_g,tab,re_bsn,tab,sdrain_bsn,tab,
     &   drain_co_bsn,tab,pc_bsn,tab,latksatf_bsn,tab

        write (575,"(*(i4,:,a))") itdrn,tab,iwtdn,tab
        write (575,"(f12.4)") sol_p_model
        write (575,"(*(i4,:,a))") iabstr,tab,iatmodep,tab
        write (575,"(*(f12.4,:,a))") r2adj,tab,sstmaxd_bsn,tab
        write (575,"(*(i4,:,a))") ismax,tab,iroutunit,tab

      end if

      if (show_section(5) == 1) then
        ! WWQ
        write(575,*) "WWQ"
        write (575,"(i4,:,a)") lao, tab
        write (575,"(i4,:,a)") igropt, tab
        write (575,"(*(f12.4,:,a))") ai0,tab,ai1,tab,ai2,tab,ai3,tab,
     &   ai4,tab,ai5,tab,ai6,tab
        write (575,"(*(f12.4,:,a))") mumax,tab,rhoq,tab,tfact,tab,
     &   k_l,tab,k_n,tab,k_p,tab,lambda0,tab,lambda1,tab,lambda2,tab
        write (575,"(*(f12.4,:,a))") p_n,tab,chla_subco,tab
      end if

      if (show_section(6) == 1) then
        ! Subbasin weather ids
        write(575,*) "Historical Weather"

        do ii = 1, msub-1
          station_ids(1) = 0
          station_ids(2) = 0
          station_ids(3) = 0
          station_ids(4) = 0
          station_ids(5) = 0
          if (pcpsim==1) station_ids (1) = irgage (ii)
          if (tmpsim==1) station_ids (2) = itgage (ii)
          if (slrsim==1) station_ids (1) = isgage (ii)
          if (hmdsim==1) station_ids (1) = ihgage (ii)
          if (wndsim==1) station_ids (1) = iwgage (ii)
          write (575,"(*(i4,:,a))") inum1s(ii), tab,
     &    station_ids(1),tab,
     &    station_ids(2),tab,
     &    station_ids(3),tab,
     &    station_ids(4),tab,
     &    station_ids(5),tab

        end do
      end if

 1000 format (39(a10))
 1001 format (a4,6x,6x,i4,31f10.4,4x,i4,f10.1,4f10.4)

 5201 format (a4,a1,a2,a1,a4,a1,a2,a1,a4,a1,a4,a1,a4,a1,a4,a1,a4)
 5200 format (i4,a1,i3,a1,f8.3,a1,i2,
     &        a1,i4,a1,i3,a1,i2,a1,f12.5,
     &        a1,f6.2,a1,f11.5,a1,f4.2,a1,f6.2,a1,f5.2,a1,i12)

      close (575)

      end 