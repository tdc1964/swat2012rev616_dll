#pragma once

// 2016-01-09 C++ declaration of swat2012
extern "C" {
	void swat2012
		(struct SWATBasinFortran *basin_dll,
		struct SWATCIOFortran *cio,
		int *soil_i,
		float *soil_f,
		char *soil_c,
		float *soil_layers,
		int *sub_i, // 9,n_sub
		float *sub_f,
		int *hru_i, // (9,n_hru)  1-#ops, 2-soil index, 3-soil layer index, 4-isep_typ, 5-isep_iyr, 6-isep_opt, 7-isep_tfail, 8-pothole flag, 9-#TCP ops
		float *hru_f, // (83,n_hru)
		int *mgt_h_i, // (38,n_hru)
		float *mgt_h_f, // (14,n_hru)
		int *mgt_i, // (7,total_op_count)
		float *mgt_f, // (7,total_op_count)
		float *wgn_f,
		float *wgn_mon_f,
		int *fig_i,
		float *fig_f,
		float *pcs_f, // (18,nhsl)
		struct SWATReservoirFortran *res,
		struct SWATLakeWaterQualityFortran *lwq,
		int *pond_i,
		float *pond_f,
		int *rte_swq_i,
		float *rte_swq_f,
		int *wwq_i, // (2)
		float *wwq_f, // (18)
		float *daily_pcp,
		float *daily_tmp,
		float *daily_slr,
		float *daily_hmd,
		float *daily_wnd,
		float *wus_f,
		struct SWATCropFortran *crop_dll,
		int n_crop,
		struct SWATFertilizerFortran *fert_dll,
		int n_fert,
		struct SWATPesticideFortran *pest_dll,
		int n_pest,
		struct SWATTillageFortran *till_structs,
		int n_till,
		struct SWATUrbanFortran *urban_dll,
		int n_urban,
		float *shyd, // (8,nhsl)
		float *outrch_d);
}
    
